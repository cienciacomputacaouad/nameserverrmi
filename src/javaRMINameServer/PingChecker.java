package javaRMINameServer;

import java.rmi.Naming;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Classe que implementa a thread que consulta se o servidor est� dispon�vel.
 * Utilizada no arquivo NameServerClient.
 * 
 * 
 * C�digo distribu�do sob a licen�a MIT, reproduzida a seguir:
 * 
 * Copyright 2018 Ricardo Santos
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */

public class PingChecker extends Thread {

	// Uma lista de todas as portas em que os servidres est�o ouvindo.
	// Se fosse mais de uma m�quina, seriam os endere�os de IP das m�quinas, mas
	// este c�digo foi feito para rodar em um �nico computador.
	public List<String> port;

	// A posi��o no �ndice que est� a porta com a qual se tentar� a conex�o.
	private int portIndex = 0;

	// Construtor, apenas inicializa a vari�vel que armazenar� todos os n�meros de
	// portas
	public PingChecker() {
		port = new ArrayList<>();
	}

	// Fun��o helper para inserir portas de servidores no array list
	public void addServer(String portaASerAdicionada) {
		port.add(portaASerAdicionada);
	}

	// Fun��o que implementa a thread de tentativas de conex�o
	@Override
	public void run() {

		int errors = 0;
		if (port.isEmpty()) {
			System.out.println("N�o h� lista de portas para tentar conex�o");
			return;
		}

		// Vamos limitar o n�mero de tentativas, para que n�o vire um loop infinito
		while (errors < portIndex * 2) {
			try {

				/*
				 * Instanciando a vari�vel que vai tratar as chamadas remotas de fun��o.
				 * Portanto, � feito o cast para o tipo apropriado.
				 * 
				 * Na chamada da fun��o � feita a concatena��o do endere�o em que se vai
				 * conectar, a porta e o nome da fun��o registrada pelo servidor.
				 */
				NameServerInterface c = (NameServerInterface) Naming
						.lookup("//127.0.0.1:" + port.get(portIndex) + "/Calc");

				System.out.println("Conectado na porta " + port.get(portIndex));

				// A conex�o foi feita, o servi�o est� respondendo.
				while (c.ping()) {

				}

			} catch (Exception e) {
				/*
				 * Esse bloco ser� invocado se o servidor parar de responder. Aqui podemos fazer
				 * o tratamento da falha de conex�o de forma apropriada e tentar contornar o
				 * problema.
				 * 
				 * Inicialmente, tenta-se na porta a seguir. Se a porta atual for a �ltima da
				 * lista, volta para o come�o.
				 */
				if (portIndex < port.size() - 1)
					portIndex++;
				else
					portIndex = 0;

				System.out.println(portIndex + " Tentando na porta " + port.get(portIndex));

				errors++;

				// Se a quantidade de tentativas for maior que uma quantidade aceit�vel, o
				// processo falha definitivamente
				// e o programa � encerrado.
				if (errors >= portIndex * 2) {
					System.out.println("Desistindo ap�s " + errors + " tentativas. Stack trace: \n\\n" + e);
					return;
				}

			}

			super.run();
		}
	}
}
