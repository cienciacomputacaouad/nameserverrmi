package javaRMINameServer;

import java.rmi.registry.Registry;

/**
 * 
 * Servidor do exemplo de sala de aula do servidor de nomes. Sequencia para
 * lan�ar os programas: 1. NameServerServer (quantas vezes quiser, em portas
 * diferentes) 2. NameServerClient
 *
 * N�o se esque�a que para rodar o servidor, tem que indicar nas configura��es
 * de inicializa��o a porta em argumento de linha de comando (no Eclipse, run -
 * configuration - arguments).
 * 
 * 
 * 
 * C�digo distribu�do sob a licen�a MIT, reproduzida a seguir:
 * 
 * Copyright 2018 Ricardo Santos
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */
public class NameServerServer {
	public static void main(String[] args) {

		if (args.length < 1) {
			System.out.println("Uso: java NameServerServer <n�mero da porta em que o servidor deve ser inicializado>");
			return;
		}

		try {
			System.out.println(args[0]);
			Registry r = java.rmi.registry.LocateRegistry
					.createRegistry(Integer.parseInt(args[0]));/* Create Registry */
			r.rebind("Calc", new NameServerRmi()); /* Binding the implementation class */
			System.out.println("Servidor pronto!");
		} catch (Exception e) {
			System.out.println("Server not connected " + e);
		}
	}
}
