package javaRMINameServer;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * 
 * Interface que declara os m�todos que regulamentam a comunica��o no RMI. Aqui
 * v�o as declara��es das fun��es, sem implementa��o
 * 
 * C�digo distribu�do sob a licen�a MIT, reproduzida a seguir:
 * 
 * Copyright 2018 Ricardo Santos
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */

public interface NameServerInterface extends Remote { /*
														 * Interface is remote interface so we extend remote class by
														 * extending remote class the interface identifies itself as an
														 * interface whose methods can be invoked by other jvm
														 */
	public boolean ping() throws RemoteException;
}
